### Chat app test task

Демо: https://chat-app-test-task.netlify.app/

Приложение может просыпаться около 10 секунд, тк находится на бесплатном сервере. 

Для запуска клиента: `npm start`

Для запуска сервера:
Создать переменную окружения со ссылкой на MobgoDB: 
`DB_URL=mongodb://localhost:27017/chatAppTestTask`
И `npm start`.

