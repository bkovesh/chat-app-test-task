import React, {useState} from 'react';
import * as Components from '../components';
import Radium from "radium";
import {Link, useHistory} from 'react-router-dom';
import {useMutation} from "@apollo/client";
import GQL from "../graphql";
import {Helmet} from "react-helmet";

const style = {
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  button: {
    margin:5,
    padding: '10px 20px',
    border: `1px solid grey`,
    backgroundColor: '#f5f5f5',
    outline: 'none',
    fontWeight: 'bold',
    fontSize: 16,
    transition: 'box-shadow ease-out 0.5s',
    cursor: 'pointer',
    ':hover': {
      boxShadow: '0px 0px 5px 0px grey',
      transition: 'box-shadow ease-out 0.3s',
    },
  }
}

function SignForm() {
  const history = useHistory();
  const [room, setRoom] = useState('')
  const [username, setUsername] = useState('')
  const [ login, { dataLogin }] = useMutation(GQL.LOGIN());

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const result = await login({ variables: { input: {
            username,
          }}})
      localStorage.setItem('username', JSON.stringify({username}))
      console.log('handleSubmit',result)
      history.push(`/chat/${room}`)
    } catch (e) {
      console.error(e);
    }
  }

  return (
  <form
  style={style.form}
  action="submit"
  onSubmit={handleSubmit}
  >

    <Helmet>
      <meta charSet="utf-8" />
      <title>Chat App. Sign in</title>
      <link rel="canonical" href="http://mysite.com/example" />
    </Helmet>


    <h1>
      Chat
    </h1>
    <h3>
      Sign in
    </h3>

    <Components.Input
    styleContainer={{margin:5,}}
    id="room"
    label="room"
    placeholder="room"
    value={room}
    onChange={(e) => setRoom(e.target.value)}
    />

    <Components.Input
    styleContainer={{margin:5,}}
    id="username"
    label="username"
    placeholder="username"
    value={username}
    onChange={(e) => setUsername(e.target.value)}
    />


    <input
    style={style.button}
    type="submit"
    value="Enter"
    />

  </form>
  );
}

export default Radium(SignForm);
