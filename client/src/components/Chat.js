import React, {useEffect} from 'react';
import {Route, Switch, Link, useRouteMatch, useParams} from "react-router-dom";
import { useQuery, useLazyQuery, useMutation, gql } from '@apollo/client';
import GQL from '../graphql';
import * as Components from '../components';
import {Helmet} from "react-helmet";


const style = {
  h1: {
    textDecoration: 'none',
    textAlign: 'center',
  },
  link: {
    textDecoration: 'none',
    color: '#000',
  },
  loading: {
    textAlign: 'center',
  },
  messages: {
    margin: 20,
    maxHeight: '60vh',
    overflow: 'auto',
  },
}


function Room() {
  let { room } = useParams();
  const {
    loading: loadingGetMessages,
    error: errorGetMessages,
    data: dataGetMessages
  } = useQuery(GQL.GET_MESSAGES(),{
    pollInterval: 1000,
    fetchPolicy: 'network-only',
    variables: { input: {
        room,
        skip: 0,
        limit: 100000
    }}
  });

  useEffect(() => {
    !loadingGetMessages && console.log('dataGetMessages',dataGetMessages)
  },[])

  useEffect(() => {
    dataGetMessages && console.log('dataGetMessages',dataGetMessages)
  },[loadingGetMessages])

  useEffect(() => {
    errorGetMessages && console.error('errorGetMessages',errorGetMessages)
  },[errorGetMessages])

  return (
  <div>

    <Helmet>
      <meta charSet="utf-8" />
      <title>Chat App #{room}</title>
      <link rel="canonical" href="http://mysite.com/example" />
    </Helmet>

    <h3 style={style.h1}>
      #{room}
    </h3>

    <div style={style.messages}>
      { loadingGetMessages ?
      <div style={style.loading}>Loading...</div> :
      ( dataGetMessages.getMessages && dataGetMessages.getMessages.length>0 ) ?
      dataGetMessages.getMessages.map((message,im) => {
        const {text, author} = message;
        return (
        <Components.Message
        key={`message-${im}`}
        text={text}
        author={author}
        />
        )
      })
      :
      <div style={style.loading}>No messages yet</div>
      }
    </div>

    <div>
      <Components.MessageForm
      room={room}
      />
    </div>
  </div>
  );
}


function Chat() {
  let match = useRouteMatch();
  return (
  <div>
    <Link
    to="/"
    style={style.link}
    >
      <h1 style={style.h1}>
        Chat
      </h1>
    </Link>
    <Switch>
      <Route exact path={`${match.path}/:room`}>
        <Room/>
      </Route>
    </Switch>
  </div>
  );
}


export default Chat;
