import React, {useState} from 'react';
import Radium from 'radium';

const style = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
  },
  label: {
    position: 'absolute',
    fontSize: 14,
    top: 0,
    left: 5,
    userSelect: 'none',
    transition: 'top ease-out 2s',
  },
  input: {
    border: `1px solid grey`,
    borderRadius: 0,
    padding: 10,
    fontSize: 16,
    fontWeight: 'bold',
    outline: 'none',
    transition: 'box-shadow ease-out 0.5s',
    ':hover': {
      border: `1px solid grey`,
      boxShadow: 'inset 0px 0px 3px 0px lightgrey',
      transition: 'box-shadow ease-out 0.3s, background-color ease-out 0.3s',
    },
    ':focus': {
      border: `1px solid grey`,
      boxShadow: 'inset 0px 0px 3px 0px grey',
      transition: 'box-shadow ease-out 0.3s, border ease-out 0.3s',
    },
  },
}

function Input(props) {
  const {
    id,
    type='text',
    styleContainer={},
    styleInput={},
    styleLabel={},
    placeholder,
    label,
    onChange,
    value='',
  } = props;


  return (
  <div
  style={{...style.container,...styleContainer}}
  >
    { label && value &&
      <label
      style={{
        ...style.label,
        ...styleLabel,
      }}
      htmlFor={id}
      >
        {label}:
      </label>
    }
    <input
    style={{
      ...style.input,
      ...styleInput,
      ...value && { padding:'15px 10px 5px 10px' },
    }}
    type={type}
    id={id}
    key={id}
    placeholder={placeholder}
    onChange={(e) => {
      onChange(e)
    }}
    value={value}
    />
  </div>
  );
}

export default Radium(Input);
