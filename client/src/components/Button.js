import React from 'react';

const style = {
  button: {
    padding: '10px 20px',
    border: `1px solid lightgrey`,
    backgroundColor: '#f1f1f1',
    outline: 'none',
    fontWeight: 'bold',
    fontSize: 16,
  }
}

function Button(props) {
  const {
    text,
    styleButton = {},
    onClick,
  } = props;
  return (
  <button
  style={{...style.button,...styleButton}}
  onClick={onClick}
  >
    {text}
  </button>
  );
}

export default Button;
