import React, {useState} from 'react';
import * as Components from '../components';
import Radium from "radium";
import {Link} from 'react-router-dom';
import {useMutation} from "@apollo/client";
import GQL from "../graphql";

const style = {
  form: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    margin:5,
    padding: '10px 20px',
    border: `1px solid grey`,
    backgroundColor: '#f5f5f5',
    outline: 'none',
    fontWeight: 'bold',
    fontSize: 16,
    transition: 'box-shadow ease-out 0.5s',
    cursor: 'pointer',
    ':hover': {
      boxShadow: '0px 0px 5px 0px grey',
      transition: 'box-shadow ease-out 0.3s',
    },
  }
}

function MessageForm(props) {
  const { room } = props;
  const [message, setMessage] = useState('')
  const [ sendMessage, { dataSendMessage }] = useMutation(GQL.SEND_MESSAGE());


  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const username = JSON.parse(localStorage.getItem('username'))
      console.log('handleSubmit',username)
      const result = await sendMessage({ variables: { input: {
            author: username.username,
            room,
            text: message,
          }}})
      console.log('handleSubmit',result)
    } catch (e) {
      console.error(e);
    }
  }

  return (
  <form
  style={style.form}
  action="submit"
  onSubmit={handleSubmit}
  >

    <Components.Input
    styleContainer={{margin:5,}}
    id="message"
    label="message"
    placeholder="message"
    value={message}
    onChange={(e) => setMessage(e.target.value)}
    />

    <input
    style={style.button}
    type="submit"
    value="Send"
    />

  </form>
  );
}

export default Radium(MessageForm);
