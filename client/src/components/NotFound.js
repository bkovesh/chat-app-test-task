import React from 'react';
import {Link} from 'react-router-dom'

const style = {
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }
}

function NotFound() {
  return (
  <div style={style.form}>
    <h1>
      Page not found
    </h1>
    <Link
    to="/"
    >
      To main page
    </Link>
  </div>
  );
}

export default NotFound;
