import React from 'react';

const style = {
  message: {
    // borderBottom: `1px solid lightgrey`,
    margin: 5,
    padding: 10,
  }
}

function Message(props) {
  const {
    text,
    author,
  } = props;
  return (
  <div style={style.message}>
    <b>{author}</b>: {text}
  </div>
  );
}

export default Message;
