import {gql} from "@apollo/client";

const GQL = {

  GET_MESSAGES () { return gql`
    query Query($input: FindMessagesInput!) {
      getMessages(input: $input) {
        ${this.MESSAGE}
      }
    }
  `},
  SEND_MESSAGE () { return gql`
    mutation Mutation($input: MessageInput!) {
      sendMessage(input: $input) {
        ${this.RESPONSE}
      }
    }
  `},



  LOGIN () { return gql`
    mutation Mutation($input: LoginInput!) {
      login(input: $input) {
        ${this.USER}
      }
    }
  `},



  USER: `
    _id
    username
  `,

  MESSAGE: `
    _id
    author
    room
    text
    created
    updated
    attachments
  `,

  RESPONSE: `
    status
    message
  `,
}

export default GQL;