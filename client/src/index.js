import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import config from './config'

const client = new ApolloClient({
  uri: `${config.urlServer}/graphql`,
  cache: new InMemoryCache(),
  onError: (e) => { console.log(e) },
  credentials: 'include',
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </ApolloProvider>
  ,document.getElementById('root')
);

