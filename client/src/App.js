import React from 'react';
import * as Components from './components';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const style = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    // justifyContent: 'center',
    height: '100vh',
  }
}

function App() {
  return (
  <Router>
    <div style={style.container}>
      <Switch>
        <Route exact path="/">
          <Components.SignForm/>
        </Route>
        <Route path="/chat">
          <Components.Chat/>
        </Route>
        <Route exact path="*">
          <Components.NotFound/>
        </Route>
      </Switch>
    </div>
  </Router>
  );
}

export default App;
