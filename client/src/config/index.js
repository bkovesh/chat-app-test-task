const isDev = process.env.NODE_ENV && process.env.NODE_ENV.trim()!=='production';

const config = {
  urlServer: isDev ? 'http://localhost:5000' : 'https://chat-app-test-task.herokuapp.com',
}

export default config;