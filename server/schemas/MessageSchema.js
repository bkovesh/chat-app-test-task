const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const MessageSchema = new Schema({
  author: {
    type: String,
    required:true,
  },
  room: {
    type: String,
    required:true,
  },
  text: {
    type: String,
    required:true,
  },
  created: {
    type: Date,
  },
  updated: {
    type: Date,
  },
  attachments: {
    type: [Schema.Types.ObjectId],
  },
});

// MessageSchema.index({ name: 'text', secondName: 'text', desc: 'text' })

module.exports = mongoose.model('Message', MessageSchema);