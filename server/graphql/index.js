const { makeExecutableSchema } = require('graphql-tools');
const Message = require('../schemas/MessageSchema');
const User = require('../schemas/UserSchema');


const typeDefs = `

  type Query {
    getMessages(input: FindMessagesInput!): [Message]
  }
  
  
  type Mutation {
    login(input: LoginInput!): User
    sendMessage(input: MessageInput!): Response!
  }
  
  
  type Message {
    _id: String
    author: String!
    room: String!
    text: String!
    created: String!
    updated: String
    attachments: [String]
  }
  
  
  type User {
    _id: String
    username: String
  }
  
  
  type Response {
    status: Int!
    message: String
  }
  
  
  input MessageInput {
    author: String!
    room: String!
    text: String!
    attachments: [String]
  }
  
  
  input LoginInput {
    room: String
    username: String
  }
  
  
  input FindMessagesInput {
    room: String
    skip: Int = 0
    limit: Int = 20
  }
  
  
`;


const resolvers = {
  Query: {
    getMessages: async (parent, {input}, ctx) => {
      let {room, skip, limit} = input;
      let result = await Message.find({room}).skip(skip).limit(limit);
      // console.log(result)
      return result;
    },
  },
  Mutation: {
    login: async (parent, {input}, ctx) => {
      try {
        const {req} = ctx;
        const {username} = input;
        // let user = await User.findOne({username});
        // if (user) return {success:403,message:"This username is already taken"};
        // const newUser = new User({username});
        req.session.username = username
        req.session.save()
        console.log('login',req.session)
        return {status:200,message:'Done',};
      } catch (e) {
        console.error(e);
        return {status:403,message:e.message,};
      }
    },

    sendMessage: async (_, {input}, ctx) => {
      try {
        // console.log('login',ctx.req.session)
        console.log('sendMessage',input)
        let {req} = ctx;
        let {room, text, author} = input;
        if (!req.session) return {status:403,message:"Session expired"};
        const message = new Message({room, text, author});
        message.created = new Date();
        await message.save();
        return {status:200,message:"Success!"};
      } catch (e) {
        console.error(e);
        return {status:403,message:e.message};
      }
    },
  },
};



const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});



module.exports = schema;