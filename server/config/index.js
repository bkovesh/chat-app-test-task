module.exports = {
  isDev: process.env.NODE_ENV && process.env.NODE_ENV.trim()!=='production' ? true : false,
  urlClient: this.isDev ? 'http://localhost:3000' : 'chat-app-test-task.netlify.app',
}