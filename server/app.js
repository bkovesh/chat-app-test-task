const createError = require('http-errors');
const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const {ApolloServer} = require('apollo-server-express');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const config = require('./config')
require('dotenv').config();


// =========================================


const app = express();

const store = new MongoDBStore({
  uri: process.env.DB_URL,
  collection: 'sessions'
});

app.use(session({
  secret: 'This is a secret',
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 // 24 hour
  },
  store: store,
  resave: false,
  saveUninitialized: false
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


const corsOptions = {
  allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'],
  credentials: true,
  // origin: config.urlClient,
  origin: (origin, callback) => {
    const whitelist = [
      config.urlClient,'http://localhost:5000'
    ];
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      console.log(origin)
      callback(new Error("Not allowed by CORS",origin))
    }
  }
}
app.use(cors(corsOptions))


// =========================================

const schema = require('./graphql/index')
const server = new ApolloServer({
  schema,
  // cors: corsOptions, // не помогает
  cors: false, // для того, чтобы Аполло не перезаписал мои настройки
  formatError: (err) => {
    // Don't give the specific errors to the client.
    if (err.message.startsWith("Database Error: ")) {
      return new Error('Internal server error');
    }
    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
  // add request and response to graphQL context
  // context: ({ req, res }) => ({ req, res }),
  context: ({ req, res }) => (req.user ? { req, res, me: req.user } : { req, res }),
});
server.applyMiddleware({
  app,
  cors: false, // для того, чтобы Аполло не перезаписал мои настройки
});


// =========================================


app.use(function(req, res, next) {
  next(createError(404));
});

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  console.log(err.message)
  res.send({message:'Server error'});
});


// =========================================


const dbOpts = {
  autoIndex: false,
  useNewUrlParser: true,
  useUnifiedTopology: true
}

mongoose.connect(process.env.DB_URL, dbOpts, (err) => {
  if(err) return console.log(err);
  app.listen(config.isDev ? 5000 : process.env.PORT, () => {
    console.log('App is running!',config.isDev, config.urlClient);
  });
});


module.exports = app;
